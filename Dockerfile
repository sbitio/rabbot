FROM golang:1.13-buster as go113_build_base
WORKDIR /go/src/app
COPY . .
RUN go get -d -v -trimpath -mod=readonly ./... \
 && go install -v -trimpath -mod=readonly ./...


FROM debian:buster

LABEL mantainer="soporte@sbit.io"
LABEL org.opencontainers.image.authors="Sbit.io <soporte@sbit.io>"

COPY --from=go113_build_base /go/bin/rabbot /usr/bin/rabbot

WORKDIR /app

ENTRYPOINT ["/usr/bin/rabbot"]
