package client

import (
	"github.com/google/uuid"
	"github.com/streadway/amqp"

	"rabbot/config"
)

func (c client) Publish(body string) {
	id := uuid.New().String()
	c.log("Publishing a message", "rabbotId", id, "routingKey", c.routingKey)
	err := c.publish(id, c.exchange.Name, c.routingKey, body, nil)
	if err == nil {
		c.log("Message published", "rabbotId", id)
	}
}

func (c client) Republish(id string, body string, retryCount int32) {
	retryCount += 1
	headers := amqp.Table{
		config.Headers.RabbotId: id,
		config.Headers.Retries:  retryCount,
	}
	exchangeName := c.retries.RetryPrefixName + c.exchange.Name

	logArgs := []interface{}{"rabbotId", id, "retryCount", retryCount, "exchange", exchangeName, "routingKey", c.queue.Name}
	c.log("Republishing a message", logArgs...)
	err := c.publish(id, exchangeName, c.queue.Name, body, headers)
	if err == nil {
		c.log("Message republished", logArgs...)
	}
}

func (c client) PublishDeadLetter(id string, body string) {
	headers := amqp.Table{
		config.Headers.RabbotId: id,
	}
	exchangeName := c.retries.DeadLetterPrefixName + c.exchange.Name

	logArgs := []interface{}{"rabbotId", id, "exchange", exchangeName, "routingKey", c.queue.Name}
	c.log("Publishing a dead letter message", logArgs...)
	err := c.publish(id, exchangeName, c.queue.Name, body, headers)
	if err == nil {
		c.log("Dead letter message published", logArgs...)
	}
}

func (c client) publish(id string, exchangeName string, routingKey string, body string, headers amqp.Table) error {
	msg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "text/plain",
		Body:         []byte(body),
		Headers:      headers,
	}
	c.debug("AMQP Message", "rabbotId", id, "message", msg, "exchange", exchangeName, "routingKey", routingKey)
	err := c.session.Push(exchangeName, routingKey, msg)
	if err != nil {
		c.warn("Failed to publish a message", "rabbotId", id, "error", err)
	}

	return err
}
