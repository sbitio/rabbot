package client

import (
	"github.com/streadway/amqp"

	"rabbot/config"
	"rabbot/logger"
	"rabbot/session"
)

type client struct {
	name        string
	session     *session.Session
	queue       config.Queue
	exchange    config.Exchange
	routingKey  string
	retries     config.Retries
	concurrency int
	command     []string
}

func New(name string, uri string, queue config.Queue, exchange config.Exchange, retries config.Retries, concurrency int, command []string) *client {
	var routingKey string
	if (config.Exchange{}) == exchange {
		// In the default exchange, the routing key matches the queue name.
		// @see https://www.rabbitmq.com/tutorials/amqp-concepts.html#exchange-default
		routingKey = queue.Name
	} else {
		routingKey = queue.Key
	}

	c := client{
		name:        name,
		queue:       queue,
		exchange:    exchange,
		routingKey:  routingKey,
		retries:     retries,
		concurrency: concurrency,
		command:     command,
	}

	c.session = session.New(uri, c.init)

	return &c
}

func failOnError(err error, msg string) {
	if err != nil {
		logger.S().Fatalw(msg, "error", err)
	}
}

func (c *client) init(conn *amqp.Connection) *amqp.Channel {
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel to AMQP server")

	err = ch.Confirm(false)
	failOnError(err, "Failed to put the channel into confirm mode")

	err = ch.Qos(
		c.concurrency, // prefetch count
		0,             // prefetch size
		false,         // global
	)
	failOnError(err, "Failed to set channel QoS")

	c.session.ChangeChannel(ch)

	//////////////////////////////////////////////////////////////////////////////
	c.session.DeclareQueue(c.queue, c.queue.Name, nil)

	if (config.Exchange{}) != c.exchange {
		c.session.DeclareExchange(c.exchange, c.exchange.Name)
		c.session.BindQueue(c.queue.Name, c.queue.Key, c.exchange.Name, c.queue.NoWait)

		// Declare retry system
		if (config.Retries{} != c.retries) {
			c.session.BindQueue(c.queue.Name, c.queue.Name, c.exchange.Name, c.queue.NoWait)

			retryExchangeName := c.retries.RetryPrefixName + c.exchange.Name
			c.session.DeclareExchange(c.exchange, retryExchangeName)

			retryQueueName := c.retries.RetryPrefixName + c.queue.Name
			c.session.DeclareQueue(c.queue, retryQueueName,
				amqp.Table{
					"x-dead-letter-exchange":    c.exchange.Name,
					"x-dead-letter-routing-key": c.queue.Name,
					"x-message-ttl":             c.retries.Ttl,
				})

			c.session.BindQueue(retryQueueName, c.queue.Name, retryExchangeName, c.queue.NoWait)

			deadLetterExchangeName := c.retries.DeadLetterPrefixName + c.exchange.Name
			c.session.DeclareExchange(c.exchange, deadLetterExchangeName)

			deadLetterQueueName := c.retries.DeadLetterPrefixName + c.queue.Name
			c.session.DeclareQueue(c.queue, deadLetterQueueName, nil)

			c.session.BindQueue(deadLetterQueueName, c.queue.Name, deadLetterExchangeName, c.queue.NoWait)
		}
	}
	//////////////////////////////////////////////////////////////////////////////

	c.log("Connection established")
	c.session.Ready()

	return nil
}

func (c client) Close() {
	c.session.Close()
	c.log("Connection closed")
}
