package client

import (
	"fmt"
	"unicode/utf8"
)

func truncate(str string, outputLen int, truncateText string) string {
	runes := []rune(str)
	runesLen := len(runes)
	if outputLen >= runesLen {
		return str
	}

	truncateTextLen := utf8.RuneCountInString(truncateText)
	beginingChunkLen := (outputLen-truncateTextLen)/2 + (outputLen-outputLen)%2
	endingChunkLen := (outputLen - truncateTextLen) / 2

	fmt.Println(outputLen, truncateTextLen, beginingChunkLen, endingChunkLen)

	result := make([]rune, outputLen)

	copy(result, runes[0:beginingChunkLen])
	copy(result[beginingChunkLen:], []rune(truncateText))
	copy(result[beginingChunkLen+truncateTextLen:], runes[runesLen-endingChunkLen:])

	return string(result)
}
