package client

import (
	"encoding/base64"
	"os/exec"

	"github.com/google/uuid"
	"github.com/streadway/amqp"

	"rabbot/config"
)

func (c client) Consume() {
	c.session.Stream(c.queue.Name, c.consumer)
}

func (c client) consumer(d amqp.Delivery) {
	var id string = ""
	for k, v := range d.Headers {
		if k == config.Headers.RabbotId {
			id = v.(string)
			break
		}
	}
	if id == "" {
		id = uuid.New().String()
	}
	c.log("Message received", "rabbotId", id)
	c.debug("AMQP Delivery", "rabbotId", id, "delivery", d)
	go c.worker(id, d)
}

func (c client) worker(id string, d amqp.Delivery) {
	body := base64.StdEncoding.EncodeToString(d.Body)
	command := append(c.command, body)
	cmd := exec.Command(command[0], command[1:]...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		truncatedOutput := truncate(string(out), config.GetAsInt("ERROR_OUTPUT_MAX_LENGTH", 700), config.Get("ERROR_OUTPUT_TRUNCATE_TEXT", "\n---TRUNCATED---\n"))
		c.error("Failed to process a delivery", "rabbotId", id, "command", command, "error", err, "output", truncatedOutput)
		if (config.Retries{} != c.retries) {
			c.retry(id, d)
		}
	} else {
		c.log("Message processed", "rabbotId", id, "output", string(out))
	}
	d.Ack(false)
}

func (c client) retry(id string, d amqp.Delivery) {
	var countRetries int32
	for k, v := range d.Headers {
		if k == config.Headers.Retries {
			countRetries = v.(int32)
		}
	}

	if countRetries < c.retries.Number {
		c.Republish(id, string(d.Body), countRetries)
	} else {
		c.PublishDeadLetter(id, string(d.Body))
	}
}
