package client

import (
	"rabbot/logger"
)

func (c client) prepareLogArgs(args []interface{}) []interface{} {
	contains := func(slice []interface{}, key string) bool {
		for _, item := range slice {
			if item == key {
				return true
			}
		}
		return false
	}
	extraArgs := map[string]string{
		"processor": c.name,
		"vhost":     c.session.GetVhost(),
		"exchange":  c.exchange.Name,
		"queue":     c.queue.Name,
	}
	for key, value := range extraArgs {
		if contains(args, key) == false {
			args = append(args, key, value)
		}
	}

	return args
}

func (c client) debug(msg string, args ...interface{}) {
	logger.S().Debugw(msg, c.prepareLogArgs(args)...)
}

func (c client) error(msg string, args ...interface{}) {
	logger.S().Errorw(msg, c.prepareLogArgs(args)...)
}

func (c client) log(msg string, args ...interface{}) {
	logger.S().Infow(msg, c.prepareLogArgs(args)...)
}

func (c client) warn(msg string, args ...interface{}) {
	logger.S().Warnw(msg, c.prepareLogArgs(args)...)
}
