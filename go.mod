module rabbot

require (
	github.com/coreos/go-systemd/v22 v22.0.0
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	go.uber.org/zap v1.14.1
	gopkg.in/yaml.v2 v2.2.7
)

replace github.com/coreos/go-systemd => github.com/coreos/go-systemd/v22 v22.0.0
