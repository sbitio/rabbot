package config

import (
	"errors"
	"log"
	"os"
	"path/filepath"

	"github.com/joho/godotenv"
)

// Config files search locations.
var searchPaths []string

func propagateSearchPaths() {
	// Cwd.
	path, _ := os.Getwd()
	searchPaths = append(searchPaths, path)

	// Path of the executable.
	path, _ = os.Executable()
	path = filepath.Dir(path)
	if searchPaths[0] != path {
		searchPaths = append(searchPaths, path)
	}

	// System path.
	path = "/etc/rabbot"
	searchPaths = append(searchPaths, path)
}

func getPathFor(filename string) (string, error) {
	for _, path := range searchPaths {
		file := path + "/" + filename
		info, err := os.Stat(file)
		if os.IsNotExist(err) {
			continue
		}
		if info.IsDir() {
			log.Printf("Error %v found but it is a directory. Ignoring!", file)
			continue
		}
		return file, nil
	}

	return "", errors.New("File not found in search paths")
}

func init() {
	propagateSearchPaths()

	path, err := getPathFor(".env")
	if err != nil {
		log.Print("No dotenv file found.")
	} else {
		log.Printf("Found dotenv file at %v", path)
		err := godotenv.Load(path)
		if err != nil {
			log.Printf("Failed to load dotenv file with error: %v", err)
		}
	}

}
