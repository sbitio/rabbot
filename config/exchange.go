package config

type Exchange struct {
	Name       string
	Type       string
	Durable    bool
	AutoDelete bool `yaml:"autoDelete"`
	Internal   bool
	NoWait     bool `yaml:"noWait"`
}
