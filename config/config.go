package config

import (
	"log"
	"os"
	"strconv"
)

func Get(key string, defaultValue ...string) string {
	if len(defaultValue) > 1 {
		log.Panic("config.Get() only accepts one default value")
	}
	value := os.Getenv(key)
	if value == "" && len(defaultValue) > 0 {
		value = defaultValue[0]
	}

	return value
}

func GetAsInt(key string, defaultValue ...int) int {
	strValue := Get(key, "")
	if strValue == "" && len(defaultValue) > 0 {
		return defaultValue[0]
	}

	value, err := strconv.ParseInt(strValue, 10, 0)
	if err != nil {
		log.Panic("config.GetAsInt() can not parse ", key)
	}

	return int(value)
}
