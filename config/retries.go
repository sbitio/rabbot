package config

type Retries struct {
	RetryPrefixName      string `yaml:"retryPrefixName"`
	DeadLetterPrefixName string `yaml:"deadLetterPrefixName"`
	Ttl                  int
	Number               int32
}
