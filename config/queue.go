package config

type Queue struct {
	Name       string
	Key        string
	Durable    bool
	AutoDelete bool `yaml:"autoDelete"`
	Exclusive  bool
	NoWait     bool `yaml:"noWait"`
}
