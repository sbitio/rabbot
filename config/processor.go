package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"

	"rabbot/logger"
)

type Processor struct {
	Name        string
	URI         string
	Concurrency int
	Queue       Queue
	Exchange    Exchange
	Command     []string
	Retries     Retries
}

type yamlContent struct {
	Processors []Processor
}

func GetProcessors() []Processor {
	y := yamlContent{}

	path, err := getPathFor("processors.yaml")
	if err != nil {
		logger.S().Fatalf("No processors.yaml file found.")
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		logger.S().Fatalf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal([]byte(data), &y)
	if err != nil {
		logger.S().Fatalf("Unmarshal: %v", err)
	}

	return y.Processors
}
