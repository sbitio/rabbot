package config

var Headers headers

type headers struct {
	Retries  string
	RabbotId string
}

func init() {
	Headers = headers{
		Retries:  "x-retries",
		RabbotId: "x-rabbotid",
	}
}
