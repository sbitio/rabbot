package session

import (
	"errors"
	"time"

	"github.com/streadway/amqp"

	"rabbot/logger"
)

type sessionInitFunc func(*amqp.Connection) *amqp.Channel

type Session struct {
	connection      *amqp.Connection
	channel         *amqp.Channel
	consumerTag     string
	done            chan bool
	notifyConnClose chan *amqp.Error
	notifyChanClose chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	ready           chan bool
	isReady         bool
	init            sessionInitFunc
}

const (
	// When reconnecting to the server after connection failure
	reconnectDelay = 5 * time.Second

	// When setting up the channel after a channel exception
	reInitDelay = 2 * time.Second

	// When resending messages the server didn't confirm
	resendDelay = 5 * time.Second
)

var (
	errNotConnected  = errors.New("not connected to a server")
	errAlreadyClosed = errors.New("already closed: not connected to the server")
	errShutdown      = errors.New("session is shutting down")
)

// New creates a new consumer state instance, and automatically
// attempts to connect to the server.
func New(addr string, init sessionInitFunc) *Session {
	session := Session{
		init:  init,
		done:  make(chan bool),
		ready: make(chan bool, 1),
	}
	go session.handleReconnect(addr)
	return &session
}

// handleReconnect will wait for a connection error on
// notifyConnClose, and then continuously attempt to reconnect.
func (session *Session) handleReconnect(addr string) {
	for {
		session.isReady = false
		logger.S().Info("Attempting to connect")

		conn, err := session.connect(addr)

		if err != nil {
			logger.S().Warn("Failed to connect. Retrying...")

			select {
			case <-session.done:
				return
			case <-time.After(reconnectDelay):
			}
			continue
		}

		if done := session.handleReInit(conn); done {
			break
		}
	}
}

// connect will create a new AMQP connection
func (session *Session) connect(addr string) (*amqp.Connection, error) {
	conn, err := amqp.Dial(addr)

	if err != nil {
		return nil, err
	}

	session.changeConnection(conn)
	return conn, nil
}

// handleReconnect will wait for a channel error
// and then continuously attempt to re-initialize both channels
func (session *Session) handleReInit(conn *amqp.Connection) bool {
	for {
		session.isReady = false

		err := session.init(conn)

		if err != nil {
			logger.S().Warn("Failed to initialize channel. Retrying...")

			select {
			case <-session.done:
				session.done <- true
				return true
			case <-time.After(reInitDelay):
			}
			continue
		}

		select {
		case <-session.done:
			session.done <- true
			return true
		case <-session.notifyConnClose:
			logger.S().Info("Connection closed. Reconnecting...")
			return false
		case <-session.notifyChanClose:
			logger.S().Info("Channel closed. Re-running init...")
		}
	}
}

// changeConnection takes a new connection to the queue,
// and updates the close listener to reflect this.
func (session *Session) changeConnection(connection *amqp.Connection) {
	session.connection = connection
	session.notifyConnClose = make(chan *amqp.Error)
	session.connection.NotifyClose(session.notifyConnClose)
}

// changeChannel takes a new channel to the queue,
// and updates the channel listeners to reflect this.
func (session *Session) changeChannel(channel *amqp.Channel) {
	session.channel = channel
	session.notifyChanClose = make(chan *amqp.Error)
	session.notifyConfirm = make(chan amqp.Confirmation, 1)
	session.channel.NotifyClose(session.notifyChanClose)
	session.channel.NotifyPublish(session.notifyConfirm)
}

// Close will cleanly shutdown the channel and connection.
func (session *Session) Close() error {
	session.done <- true
	if !session.isReady {
		return errAlreadyClosed
	}

	logger.S().Info("Closing channel consumers...")
	err := session.channel.Cancel(session.consumerTag, false)
	if err != nil {
		return err
	}
	logger.S().Info("Closing channel...")
	err = session.channel.Close()
	if err != nil {
		return err
	}
	logger.S().Info("Closing connection...")
	err = session.connection.Close()
	if err != nil {
		return err
	}
	close(session.ready)
	close(session.done)
	session.isReady = false
	return nil
}
