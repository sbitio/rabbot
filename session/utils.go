package session

import (
	"os"
	"time"

	"github.com/streadway/amqp"

	"rabbot/config"
	"rabbot/logger"
)

func failOnError(err error, msg string) {
	if err != nil {
		logger.S().Fatalw(msg, "error", err)
	}
}

func (session *Session) GetVhost() string {
	return session.connection.Config.Vhost
}

// This is just a wrapper to not alter the main code copied from the amqp example.
func (session *Session) ChangeChannel(channel *amqp.Channel) {
	session.changeChannel(channel)
}

func (session *Session) Ready() {
	session.isReady = true
	session.ready <- true
}

func (session *Session) DeclareQueue(queue config.Queue, name string, args amqp.Table) {
	_, err := session.channel.QueueDeclare(
		name,
		queue.Durable,
		queue.AutoDelete,
		queue.Exclusive,
		queue.NoWait,
		args,
	)
	failOnError(err, "Failed to declare a queue")
}

func (session *Session) DeclareExchange(exchange config.Exchange, name string) {
	err := session.channel.ExchangeDeclare(
		name,
		exchange.Type,
		exchange.Durable,
		exchange.AutoDelete,
		exchange.Internal,
		exchange.NoWait,
		nil,
	)
	failOnError(err, "Failed to declare an exchange")
}

func (session *Session) BindQueue(queueName string, routingKey string, exchangeName string, noWait bool) {
	err := session.channel.QueueBind(
		queueName,
		routingKey,
		exchangeName,
		noWait,
		nil,
	)

	failOnError(err, "Failed to bind a queue")
}

// Push will push data onto the queue, and wait for a confirm.
// If no confirms are received until within the resendTimeout,
// it continuously re-sends messages until a confirm is received.
// This will block until the server sends a confirm. Errors are
// only returned if the push action itself fails, see UnsafePush.
func (session *Session) Push(exchangeName string, routingKey string, msg amqp.Publishing) error {
	for {
		err := session.UnsafePush(exchangeName, routingKey, msg)
		if err != nil {
			logger.S().Warn("Push failed. Retrying...")
			select {
			case <-session.done:
				return errShutdown
			case <-time.After(resendDelay):
			}
			continue
		}
		select {
		case confirm := <-session.notifyConfirm:
			if confirm.Ack {
				logger.S().Info("Push confirmed!")
				return nil
			}
		case <-time.After(resendDelay):
		}
		logger.S().Warn("Push didn't confirm. Retrying...")
	}
}

// UnsafePush will push to the queue without checking for
// confirmation. It returns an error if it fails to connect.
// No guarantees are provided for whether the server will
// recieve the message.
func (session *Session) UnsafePush(exchangeName string, routingKey string, msg amqp.Publishing) error {
	if !session.isReady {
		return errNotConnected
	}
	return session.channel.Publish(
		exchangeName,
		routingKey,
		false, // Mandatory
		false, // Immediate
		msg,
	)
}

// Stream will continuously put queue items on the channel.
// It is required to call delivery.Ack when it has been
// successfully processed, or delivery.Nack when it fails.
// Ignoring this will cause data to build up on the server.
func (session *Session) Stream(queueName string, consumer func(amqp.Delivery)) {
	i := 0
	if !session.isReady {
		select {
		case <-session.done:
			return
		case <-session.ready:
		}
	}

	for {
		hostname, _ := os.Hostname()
		executable, _ := os.Executable()
		consumerTag := hostname + "-" + executable + "#" + string(i)
		logger.S().Infow("Attempting to register a consumer", "consumerTag", consumerTag)

		deliveries, err := session.UnsafeStream(queueName, consumerTag)

		if err != nil {
			logger.S().Warnw("Failed to register a consumer. Retrying...", "error", err)

			select {
			case <-session.done:
				return
			case <-time.After(reInitDelay):
				continue
			}
		}
		session.consumerTag = consumerTag
		logger.S().Infow("Consumer registered", "consumerTag", consumerTag)

		for {
			select {
			case d := <-deliveries:
				consumer(d)
				continue
			case <-session.done:
				return
			case <-session.ready:
				logger.S().Infow("Consumer detached", "consumerTag", consumerTag)
				i++
			}
			break
		}
	}
}

func (session *Session) UnsafeStream(queueName string, consumerTag string) (<-chan amqp.Delivery, error) {
	if !session.isReady {
		return nil, errNotConnected
	}
	return session.channel.Consume(
		queueName,
		consumerTag, // consumer
		false,       // autoAck
		false,       // exclusive
		false,       // noLocal
		false,       // noWait
		nil,         // args
	)
}
