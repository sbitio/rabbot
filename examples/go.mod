module example

require (
	github.com/joho/godotenv v1.3.0
	go.uber.org/zap v1.14.1 // indirect
)
