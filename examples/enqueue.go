package main

import (
	"os"
	"strings"
	"time"

	"rabbot/client"
	"rabbot/config"
	"rabbot/logger"
)

func main() {
	logger.New(config.Get("logLevel", "info"))
	defer logger.Close()

	queue := os.Args[1]
	body := strings.Join(os.Args[2:], " ")

	// Instantiate a client for the processor matching the given queue and publish.
	processors := config.GetProcessors()
	for _, processor := range processors {
		if processor.Queue.Name == queue {
			c := client.New(processor.Name, processor.URI, processor.Queue, processor.Exchange, processor.Retries, processor.Concurrency, processor.Command)
			time.Sleep(time.Second * 3)
			c.Publish(body)
			defer c.Close()
			break
		}
	}
}
