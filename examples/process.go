package main

import (
	"encoding/base64"
	"log"
	"os"
	"strings"
)

func main() {
	body := strings.Join(os.Args[1:], " ")
	log.Printf("Processing '%s'.", body)

	encoded := os.Args[len(os.Args)-1]
	msg, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		log.Fatal("Decode error:", err)
	}

	log.Printf("Message: '%s'", msg)
}
