package logger

import (
	"log"

	"go.uber.org/zap"
)

var L *zap.Logger

func New(level string) {
	var zapAtomicLevel zap.AtomicLevel = zap.NewAtomicLevel()
	zapAtomicLevel.UnmarshalText([]byte(level))

	var cfg zap.Config = zap.Config{
		Level:            zapAtomicLevel,
		Development:      false,
		Encoding:         "console",
		EncoderConfig:    zap.NewDevelopmentEncoderConfig(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	var err error
	L, err = cfg.Build()
	if err != nil {
		log.Fatal("Error initializing logger", err)
	}
}

func S() *zap.SugaredLogger {
	return L.Sugar()
}

func Close() {
	L.Sync()
}
