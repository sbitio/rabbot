## Introducción

Rabbot es un programa que establece N conexiones contra RabbitMQ para escuchar
en las colas indicadas y pasar los mensajes recibidos a un comando encargado
de procesarlas (comando procesador).

El mensaje se pasa al procesador como último argumento, en formato base64.

Los parámetros para establecer conexiones y el comando al que invocar se
definen en el fichero `processors.yaml`. La key de la uri de conexión corresponde
con una variable de entorno. Es decir, para: `uri: 'RABBITMQ_URI__EXAMPLE'` se espera
encontrar en `.env` una key `RABBITMQ_URI__EXAMPLE` con una uri de conexión AMQP.

Se proporciona una configuración ejemplo de processors en el fichero
`processors.yaml.example`, así como sendos ejemplos de código productor
(`examples/enqueue.go`) y código procesador (`examples/process.go`) para mostrar
el funcionamiento completo de Rabbot sin necesidad de otros artefactos salvo
un servidor RabbitMQ.


## Docker

El repositorio contiene un `Dockerfile` para construir y ejecutar Rabbot.
Un job de Gitlab CI (ver `.gitlab-ci.yml`) hace build de la imagen y la
publica en el Container Registry del proyecto.

También se proporciona un ficheros Compose (`docker-compose.yml`) que define
los servicios y configuración necesarias para lanzar sendos contenedores
RabbitMQ y Rabbot, listos para usar los processors de ejemplo.

Este fichero Compose:
 * Utiliza la imagen Rabbot del container registry de gitlab
 * Almacena los volumenes de RabbitMQ en el directorio `docker-compose-volumes`
 * Expone los puertos de RabbitMQ. Se puede acceder a la interfaz
 administrativa en http://localhost:15672


## Ejemplo de uso

### Uso con servicio RabbitMQ e instalacion de Go local

```bash
cp processors.yaml.example processors.yaml
cp .env.example .env
# Editar .env y configurar datos de conexión válidos
go run rabbot.go

go run examples/enqueue.go Q1 foo
go run examples/enqueue.go Q1 bar
go run examples/enqueue.go Q1 baz
```

### Uso con Docker

Para arrancar los servicios:

```bash
docker-compose up -d
docker-compose logs -f
```

Para parar los servicios y destruir los contenedores:

```bash
docker-compose down -v
```

## Desarrollo

Para facilitar desarrollo y pruebas en local, se proporciona un fichero Compose
específico (`docker-compose.development.yml`), que referencia al Dockerfile local
en vez de la imagen Rabbot en el registry de gitlab.


### Ejemplo de uso

```
docker-compose -f docker-compose.yml -f docker-compose.development.yml build
docker-compose -f docker-compose.yml -f docker-compose.development.yml up -d
```
