package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/coreos/go-systemd/v22/daemon"
	"github.com/coreos/go-systemd/v22/util"

	"rabbot/client"
	"rabbot/config"
	"rabbot/logger"
)

func main() {
	forever := make(chan bool)

	// Setup signal catching.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		s := <-sigs
		logger.S().Infof("RECEIVED SIGNAL: %s", s)
		forever <- false
	}()

	// Setup logger.
	logger.New(config.Get("logLevel", "info"))
	defer logger.Close()

	// Instantiate a client for each processor.
	processors := config.GetProcessors()
	for _, processor := range processors {
		uri := config.Get(processor.URI)
		if uri == "" {
			logger.S().Fatalf("Unknown uri «%v» for processor «%v». Check your dotenv!", processor.URI, processor.Name)
		}
		c := client.New(processor.Name, uri, processor.Queue, processor.Exchange, processor.Retries, processor.Concurrency, processor.Command)
		defer c.Close()
		go c.Consume()
	}

	// We're ready.
	fromSystemd, _ := util.RunningFromSystemService()
	if fromSystemd {
		daemon.SdNotify(false, "READY=1")
	} else {
		logger.S().Info(" [*] Waiting for messages. To exit press CTRL+C")
	}

	<-forever
}
